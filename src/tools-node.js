const fs = require('fs')
const path = require('path')
const net = require('net')
const request = require('request')
const os = require('os')
const crypto = require('crypto')
const childProcess = require('child_process')
const iconv = require('iconv-lite')
let toolsPublic = require('./tools-public.js')

/**
 * JSON文件读取
 * @param {String} filePath 源地址
*/
const getJson = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', function (err, data) {
            if (err) {
                reject('文件不存在或读取失败')
            } else {
                resolve(toolsPublic.jsonParse(data))
            }
        })
    })
}

/**
 * Buffer转ArrayBuffer
 * @param {String} buf Buffer数据
*/
const Buffer2ArrayBuffer = (buf) => {
    let ab = new ArrayBuffer(buf.length)
    let view = new Uint8Array(ab)
    for (let i = 0; i < buf.length; i++) {
        view[i] = buf[i]
    }
    return ab
}

/**
 * 文件拷贝
 * @param {String} from 源地址
 * @param {String} to 目标地址
 * @param {boolean} streammode 是否使用流传输
 */
const copyFile = (from, to, streammode = false) => {
    // 检查源文件是否存在
    if (!fs.existsSync(from)) return
    // 检查目标文件夹是否存在
    let toDir = path.dirname(to)
    if (!fs.existsSync(toDir)) {
        fs.mkdirSync(toDir, {recursive: true})
    }
    // 读取源文件 & 写入新文件
    if (streammode) {
        // 适用于大文件
        fs.createReadStream(from).pipe(fs.createWriteStream(to))
    } else {
        // 适用于小文件
        fs.writeFileSync(to, fs.readFileSync(from))
    }
}

/**
 * 目录拷贝
 * @param {String} from 源地址
 * @param {String} to 目标地址
 * @param {boolean} replace 是否覆盖同名文件
 */
const copyDir = (from, to, replace = true) => {
    // 检查源路径是否存在
    if (!fs.existsSync(from)) return
    // 检查源路径是否是文件夹
    let fromStat = fs.lstatSync(from)
    if (fromStat.isFile()) {
        copyFile(from, to)
        return
    }
    // 检查目标路径是否存在
    let needCreate = true
    if (fs.existsSync(to)) {
        // 检查目标路径是否是文件夹
        let toStat = fs.lstatSync(to)
        if (toStat.isDirectory()) {
            needCreate = false
        }
    }
    if (needCreate) {
        fs.mkdirSync(to, {recursive: true})
    }
    // 开始遍历拷贝
    let files = fs.readdirSync(from)
    files.forEach(v => {
        let fromPath = path.join(from, v)
        let toPath = path.join(to, v)
        let fromStat = fs.lstatSync(fromPath)
        if (fromStat.isFile()) {
            if (!fs.existsSync(toPath) || replace) {
                copyFile(fromPath, toPath)
            }
        } else if (fromStat.isDirectory()) {
            copyDir(fromPath, toPath, replace)
        }
    })
}

/**
 * 创建目录
 * @param {String} filePath 目标路径
 */
const mkDir = (filePath) => {
    // 检查目标路径是否存在
    let needCreate = true
    if (fs.existsSync(filePath)) {
      // 检查目标路径是否是文件夹
      let pathStat = fs.lstatSync(filePath)
      if (pathStat.isDirectory()) {
        needCreate = false
      }
    }
    if (needCreate) {
      fs.mkdirSync(filePath, {recursive: true})
    }
}

/**
 * 删除文件/文件夹
 * @param {String} filePath 目标路径
 */
const delFile = (filePath) => {
    // 检查源文件是否存在
    if (!fs.existsSync(filePath)) return
    let stat = fs.statSync(filePath)
    if (stat.isDirectory()) {
        let files = fs.readdirSync(filePath)
        if (files.length) {
            files.forEach(file => {
                delFile(path.join(filePath, file))
            })
        }
        fs.rmdirSync(filePath)
    } else {
        fs.unlinkSync(filePath)
    }
}

/**
 * 将数据写入文件
 * @param {String} filePath 目标路径
 * @param {Buffer} bufferData 数据
 */
const writeFileSync = (filePath, bufferData) => {
    // 检查目标文件夹是否存在
    let toDir = path.dirname(filePath)
    if (!fs.existsSync(toDir)) {
        fs.mkdirSync(toDir, {recursive: true})
    }
    fs.writeFileSync(filePath, bufferData)
}

/**
 * 将数据写入文件
 * @param {String} filePath 目标路径
 * @param {Buffer} bufferData 数据
 */
const writeFile = (filePath, bufferData) => {
    return new Promise((resolve, reject) => {
        // 检查目标文件夹是否存在
        let toDir = path.dirname(filePath)
        if (!fs.existsSync(toDir)) {
            fs.mkdirSync(toDir, {recursive: true})
        }
        fs.writeFile(filePath, bufferData, (err) => {
            if (err) {
                reject()
            } else {
                resolve()
            }
        })
    })
}

/**
 * 将分片数据异步写入文件
 * @param {Buffer} taskList Promise数组，每个Promise返回一个Buffer数据
 * @param {String} filePath 文件存储路径
 */
const writeFileV2 = (taskList, filePath) => {
    taskList = taskList || []
    let dataList = []
    let curIndex = 0
    return new Promise((resolve, reject) => {
        if (!filePath) {reject('存储地址不能为空')}
        try {
            let writeStream = fs.createWriteStream(filePath)
            // eslint-disable-next-line no-inner-declarations
            function writeData () {
                if (curIndex >= taskList.length) {
                    writeStream.end()
                    return resolve()
                }
                if (!dataList[curIndex]) return
                writeStream.write(dataList[curIndex], () => {
                    dataList[curIndex] = null
                    curIndex++
                    writeData()
                })
            }
            taskList.forEach((v, i) => {
                v.then(res => {
                    dataList[i] = res || new Buffer([])
                    writeData()
                })
            })
        } catch (error) {
            reject(error)
        }
    })
}

/**
 * 读取目录
 * @param {String} filePath 目标路径
 */
const readDir = (filePath) => {
    return new Promise((resolve, reject) => {
        if (!filePath || !fs.existsSync(filePath)) {
            reject('')
        } else {
            fs.readdir(filePath, (err, files) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(files)
                }
            })
        }
    })
}

/**
 * 获取文件MD5值
 * @param {String} filePath 目标路径
 */
const getFileMd5 = (filePath) => {
    if (!filePath || !fs.existsSync(filePath)) return ''
    try {
        let buffer = fs.readFileSync(filePath)
        let fsHash = crypto.createHash('md5')
        fsHash.update(buffer)
        return fsHash.digest('hex')
    } catch(err) {
        return ''
    }
}

/**
 * 上传文件到服务器
 * @param {String} from 源地址
 * @param {String} to 目标地址
 * @param {String} contentType 数据类型
 */
const uploadFile = ({from, to, contentType}) => {
    return new Promise((resolve, reject) => {
        request.put({
            url: to,
            headers: {
                'Content-Type': contentType || 'application/octet-stream'
            },
            body: fs.readFileSync(from)
        }, (error, response, body) => {
            if (error) reject(error)
            if (response.statusCode === 200) {
                resolve(response)
            } else {
                reject(response)
            }
        })
    })
}

/**
 * 下载文件到本地
 * @param {String} from 源地址
 * @param {String} to 目标地址
 * @param {Function} progressCallback 进度回调函数
 * @param {Object} config 其它配置
 */
const downloadFile = (options) => {
    let {from, to, progressCallback, config} = options || {}
    config = config || {}
    return new Promise((resolve, reject) => {
        // 检查目标文件夹是否存在
        let toDir = path.dirname(to)
        if (!fs.existsSync(toDir)) {
            fs.mkdirSync(toDir, {recursive: true})
        }
        toolsPublic.downloadInstance({
            url: from,
            responseType: 'blob',
            onDownloadProgress: progressCallback
        }).request(config).then((response) => {
            response.data.arrayBuffer().then((res) => {
                writeFileSync(to, Buffer.from(res))
                resolve('success')
            }).catch((err) => {
                console.log(err)
                reject('error')
            })
        }).catch((err) => {
            console.log(err)
            reject('error')
        })
    }).catch((err) => {
        console.log(err)
        return 'error'
    })
}

/**
 * 下载文件到本地
 * @param {String} from 源地址
 * @param {String} to 目标地址
 * @param {Function} progressCallback 进度回调函数
 * @param {Object} config 其它配置
 */
const downloadFileV2 = (options) => {
    let {from, to, progressCallback, config} = options || {}
    config = config || {}
    return new Promise((resolve, reject) => {
        // 检查目标文件夹是否存在
        let toDir = path.dirname(to)
        if (!fs.existsSync(toDir)) {
            fs.mkdirSync(toDir, {recursive: true})
        }
        toolsPublic.downloadInstance({
            url: from,
            onDownloadProgress: progressCallback
        }).request(config).then((res) => {
            fs.writeFileSync(to, Buffer.from(res.data))
            resolve('success')
        }).catch((err) => {
            console.log(err)
            reject('error')
        })
    }).catch((err) => {
        console.log(err)
        return 'error'
    })
}

/**
 * 下载文件到本地
 * @param {String} from 源地址
 * @param {String} to 目标地址
 */
const downloadFileV3 = (options) => {
    let {from, to} = options || {}
    return new Promise((resolve, reject) => {
        // 检查目标文件夹是否存在
        let toDir = path.dirname(to)
        if (!fs.existsSync(toDir)) {
            fs.mkdirSync(toDir, {recursive: true})
        }
        // 声明临时文件
        let toTemp = `${to}_temp`
        let stream = fs.createWriteStream(toTemp)
        request(from).on('error', (err) => {
            console.log(err)
            reject('error')
        }).pipe(stream).on('close',() => {
            fs.rename(toTemp, to, function (err) {
                if (err) { reject('error') }
                else { resolve('success') }
            })
        })
    }).catch((err) => {
        console.log(err)
        return 'error'
    })
}

/**
 * 加密数据（解密同）
 * @param {String} key 密钥
 * @param {Buffer} bufferData 数据
 */
const encrypt = (key, buffer) => {
    let cipher = crypto.createCipher('aes-256-ctr', key)
    let encryptedBytes = cipher.update(buffer)
    encryptedBytes = Buffer.concat([encryptedBytes, cipher.final()])
    return encryptedBytes
}
/**
 * 压缩&加密课件资源包
 * @param {String} from 待压缩加密的ZIP文件或文件夹
 * @param {String} to 压缩&加密后文件存放路径
 * @param {Function} JSZip jszip第三方库方法
 * @param {Buffer} encryptKey 加密key
 */
const zipFile = (options = {}) => {
    options = options || {}
    let from = options.from || ''
    let to = options.to || ''
    let JSZip = options.JSZip || (typeof window !== 'undefined' ? window.JSZip : null)
    let encryptKey = options.encryptKey
    // const encryptKey = Buffer.from('l6fwZHdJbU2Y4jxPDwjoI6P9vltHhc8bvDlExm4vRRg=', 'base64')
    if (!from) return Promise.reject('待压缩加密的文件路径不能为空')
    if (!JSZip) return Promise.reject('need input plugin method: JSZip')
    let fileName = path.basename(from.replace(/\\/g, '/'))
    let filePath = path.join(to, `${fileName}.zip_encrypted`)
    let taskList = []
    let zip = new JSZip()
    let fileStat = fs.statSync(from)
    if (fileStat.isDirectory()) {
        zipDir(zip, from)
        function zipDir (zipObject, dirPath) {
            let files = fs.readdirSync(dirPath)
            files.forEach(fName => {
                let fPath = path.join(dirPath, fName)
                let fStat = fs.statSync(fPath)
                if (fStat.isDirectory()) {
                    zipObject = zipObject.folder(fName)
                    zipDir(zipObject, fPath)
                } else {
                    taskList.push(zipObject.file(fName, fs.readFileSync(fPath)))
                }
            })
        }
    } else {
        let bytes = fs.readFileSync(from)
        taskList.push(zip.loadAsync(bytes))
    }
    return Promise.all(taskList).then(() => {
        return zip.generateAsync({
            type: 'nodebuffer',
            compression: 'DEFLATE',
            compressionOptions: { level: 1 }
        }).then((content) => {
            content = encrypt(encryptKey, content)
            writeFileSync(filePath, content)
        })
    })
}
/**
 * 解压加密课件资源包
 * @param {String} from 压缩包路径
 * @param {String} to 解压后文件存放路径
 * @param {Function} JSZip jszip第三方库方法
 * @param {Buffer} encryptKey 加密key
 */
const unzipFile = (options = {}) => {
    options = options || {}
    let zipFilePath = options.from || ''
    let unzipFileDir = options.to || ''
    let JSZip = options.JSZip || (typeof window !== 'undefined' ? window.JSZip : null)
    let encryptKey = options.encryptKey
    // const encryptKey = Buffer.from('l6fwZHdJbU2Y4jxPDwjoI6P9vltHhc8bvDlExm4vRRg=', 'base64')
    if (!zipFilePath) return Promise.reject('压缩包路径不能为空')
    if (!JSZip) return Promise.reject('need input plugin method: JSZip')
    let fileName = path.basename(zipFilePath.replace(/\\/g, '/'))
    let fileNameArr = fileName.split('.')
    if (fileNameArr.length > 1) {
        fileName = fileNameArr.slice(0, fileNameArr.length - 1).join('.')
    }
    unzipFileDir = path.join(unzipFileDir, fileName)
    let zip = new JSZip()
    let bytes = fs.readFileSync(zipFilePath)
    if (encryptKey) {
        bytes = encrypt(encryptKey, bytes)
    }
    return zip.loadAsync(bytes).then(({files}) => {
        // 将数据写入本地文件
        let arr = []
        zip.forEach((relativePath, file) => {
            arr.push(file.async('nodebuffer').then((content) => {
                if (file.dir) {
                    let filepath = path.resolve(unzipFileDir, relativePath)
                    if (!fs.existsSync(filepath)) {
                        fs.mkdirSync(filepath, {recursive: true})
                    }
                } else {
                    let filepath = path.resolve(unzipFileDir, relativePath)
                    let filedir = path.dirname(filepath)
                    if (!fs.existsSync(filedir)) {
                        fs.mkdirSync(filedir, {recursive: true})
                    }
                    fs.writeFileSync(filepath, content)
                }
            }))
        })
        return Promise.all(arr)
    })
}

/**
 * 获取本机IP地址
 */
const getIpAddress = () => {
    let result = []
    let interfaces = os.networkInterfaces() || []
    for (let devName in interfaces) {
        let iface = interfaces[devName]
        for (let i = 0; i < iface.length; i++) {
            let item = iface[i]
            if (item.family === 'IPv4' && item.address !== '127.0.0.1' && !item.internal) {
                // return item.address
                result.push(item.address)
            }
        }
    }
    return result
}

/**
 * 检测端口是否被占用
 * @param {Number} port 端口号
 */
const portIsOccupied = (port) => {
    return new Promise((resolve, reject) => {
        let server = net.createServer().listen(port)
        server.on('listening', () => {
            server.close()
            console.log(`The port【${port}】 is available.`)
            resolve(true)
        })
        server.on('error', (err) => {
            if (err.code === 'EADDRINUSE') {
                console.log(`The port【${port}】 is occupied, please change other port.`)
            } else {
                console.log(`The port【${port}】 is inadvisable.`, err)
            }
            reject(false)
        })
    })
}

/**
 * 获取可使用的端口号
 * @param {Number} s_port 起始端口号
 * @param {Number} count 需要获取的端口数量
 */
const getAvailablePorts = (s_port = 1, count = 1) => {
    s_port = Math.abs(parseInt(s_port) || 1)
    count = Math.abs(parseInt(count) || 1)
    let result = []
    return new Promise( async (resolve, reject) => {
        while ((s_port < 65535) && result.length < count) {
            let isOk = await portIsOccupied(s_port)
            if (isOk) { result.push(s_port) }
            s_port++
        }
        resolve(result)
    })
}

/**
 * 使用ping命令测算网速
 * @param {String} options/website 要ping的网站
 * @param {Number} options/bytecount 字节数
 * @param {String} options/encode 编码处理
 * @param {String} options/decode 解码处理
 */
const getNetspeedByPing = (options) => {
    iconv.skipDecodeWarning = true
    let website = options.website || 'baidu.com'
    let bytecount = options.bytecount || 1000
    let decode = options.decode || 'cp936'
    let encode = options.encode || 'binary'
    return new Promise((resolve, reject) => {
        childProcess.exec(
            `ping ${website} -l ${bytecount}`,
            {encoding: encode},
            (err, stdout, stderr) => {
                if (err) {
                    reject(iconv.decode(err, decode))
                } else {
                    let data = iconv.decode(stdout, decode)
                    let time = (
                        data.match(/平均 = (\d+)ms/) ||
                        data.match(/Average = (\d+)ms/) ||
                        []
                    )[1]
                    let speed = bytecount / time
                    if (isNaN(speed)) {
                        reject('未匹配到时长数据')
                    } else {
                        resolve(`${speed.toFixed(2)}KB/s`)
                    }
                }
            }
        )
    })
}

/**
 * 获取路由接口列表
 * @param {String} options/encode 编码处理
 * @param {String} options/decode 解码处理
 * @param {Array} options/blacklist 黑名单，排除名称包含非法字符的网卡
 */
 const getRouteInterfaceList = (options = {}) => {
    iconv.skipDecodeWarning = true
    let decode = options.decode || 'cp936'
    let encode = options.encode || 'binary'
    let blacklist = options.blacklist || []
    let result = []
    // 获取os模块网络接口信息
    let networkInterfacesDict = {}
    let interfaces = os.networkInterfaces() || []
    for (let devName in interfaces) {
        let iface = interfaces[devName]
        for (let i = 0; i < iface.length; i++) {
            let item = iface[i]
            if (item.family === 'IPv4' && item.address !== '127.0.0.1' && !item.internal) {
                networkInterfacesDict[item.mac] = item
            }
        }
    }
    return new Promise((resolve, reject) => {
        childProcess.exec(
            `route print`,
            {encoding: encode},
            (err, stdout, stderr) => {
                if (err) {
                    reject(iconv.decode(err, decode))
                } else {
                    let data = iconv.decode(stdout, decode) || ''
                    let rows = data.split('\n')
                    let rowIndex = 0
                    let equalSignIndex = -1
                    while (equalSignIndex < 2 && rowIndex < rows.length) {
                        let rowData = rows[rowIndex] || ''
                        // 处理行数据
                        let dot3start = rowData.indexOf('...')
                        let dot6start = rowData.lastIndexOf('......')
                        if (dot3start > -1 && dot6start > -1) {
                            let id = rowData.substring(0, dot3start).trim()
                            let mac = (rowData.substring(dot3start + 3, dot6start).trim()).split(' ').join(':')
                            let name = rowData.substring(dot6start + 6).trim()
                            let isInBlacklist = blacklist.some(v => name.includes(v))
                            !isInBlacklist && result.push({
                                ...(networkInterfacesDict[mac] || {}),
                                id, name, mac
                            })
                        }
                        // 修改条件变量
                        if (rowData.includes('=====')) equalSignIndex++
                        rowIndex++
                    }
                    resolve(result)
                }
            }
        )
    })
}

let toolsNode = {
    ...toolsPublic,
    getJson,
    Buffer2ArrayBuffer,
    copyFile,
    copyDir,
    mkDir,
    delFile,
    writeFileSync,
    writeFile,
    writeFileV2,
    readDir,
    getFileMd5,
    uploadFile,
    downloadFile,
    downloadFileV2,
    downloadFileV3,
    encrypt,
    zipFile,
    unzipFile,
    getIpAddress,
    portIsOccupied,
    getAvailablePorts,
    getNetspeedByPing,
    getRouteInterfaceList
}

module.exports = (jstools) => {
    for (let k in toolsNode) {
        jstools[k] = toolsNode[k]
    }
}
