  该类方法仅能在WEB环境下使用。

## downloadFileByA

  使用a标签下载文件（跨域时自定义命名无效）。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 源地址 | true | String |  |
| options.to | 目标地址/文件名 | false | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.downloadFileByA({from: 'http://www.x.com/1.pdf', to: '1.pdf'})
```

## downloadFileByAV2

  基本功能同downloadFileByA，区别是使用了XMLHttpRequest，以便在跨域环境下a标签的download改名可以正常使用。

## downloadFileByIframe

  使用iframe标签下载文件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 源地址 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.downloadFileByIframe({from: 'http://www.x.com/1.pdf'})
$jstools.downloadFileByIframe('http://www.x.com/1.pdf')
```

## downloadFileByForm

  使用Form标签下载文件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 源地址 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.downloadFileByForm({from: 'http://www.x.com/1.pdf'})
```

## saveFileByBlob

  保存Blob数据流到文件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.data | Blob数据流 | true | Blob |  |
| options.fileName | 文件名 | true | String | |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.saveFileByBlob({
    data: new Blob(),
    fileName: 'file'
})
```

## searchVueComponent

  控制台调试Vue项目:定位Vue组件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| vueObj | vue实例（搜索会在该实例下进行） | true | Object |  |
| className | 要查找的组件的根类名 | true | String | |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Object | 返回一个包含了目标组件路径的_uid数组，以及组件实例 | {path: [], component: null} |

* 代码示例
```js
$jstools.searchVueComponent(new Vue({...}), 'cmp-class')

```

## downloadFile

  axios下载文件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 源地址 | true | String |  |
| options.to | 目标地址/文件名 | false | String |  |
| progressCallback | 进度回调函数 | false | Function |  |
| config | 其它配置 | false | Object |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个axios实例 |  |

* 代码示例
```js
$jstools.downloadFile({from: ''})
```

## formatNumber

  Number转千位分隔符字符串。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| num | 数字 | true | Number |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 千位分隔符字符串 |

* 代码示例
```js
let result = $jstools.formatNumber(123456.7)
console.log('千位分隔符字符串：', result)
```

## runPrefixMethod

  执行带浏览器前缀的方法。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| element | DOM元素 | true | Object |  |
| method | 方法名 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| * | 函数返回值 |  |

* 代码示例
```js
$jstools.runPrefixMethod(window, 'getSelection')
```

## onPrefixEvent

  绑定带浏览器前缀的事件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| element | DOM元素 | true | Object |  |
| eventName | 事件名称 | true | String |  |
| callback | 事件回调函数 | true | Function |  |
| capture | 是否在捕获阶段触发 | false | Boolean |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 实际绑定的可能带前缀的事件名称 |  |

* 代码示例
```js
let fn = () => {}
$jstools.onPrefixEvent(document, 'fullscreenchange', fn)
```

## offPrefixEvent

  解绑带浏览器前缀的事件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| element | DOM元素 | true | Object |  |
| eventName | 事件名称 | true | String |  |
| callback | 事件回调函数 | true | Function |  |
| capture | 是否在捕获阶段触发 | false | Boolean |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
let fn = () => {}
$jstools.offPrefixEvent(document, 'fullscreenchange', fn)
```

## parseURLByA

  利用a标签自动解析URL。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| url | 网址 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Object | 解析后的数据集合 |  |

* 代码示例
```js
$jstools.parseURLByA('http://x.com/y/z?a=1&b=2')
```

## scrollTop

  滚动条垂直滚动。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| element | DOM元素 | true | Object |  |
| from | 起点 | true | Number | 0 |
| to | 终点 | true | Number |  |
| duration | 滚动时长 | false | Number | 500 |
| endCallback | 滚动结束后的回调函数 | false | Function |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.scrollTop(document.documentElement, 0, 300)
```

## isFullscreen

  判断是否处在全屏状态。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Boolean | 是/否 |  |

* 代码示例
```js
console.log($jstools.isFullscreen())
```

## isFullscreenEnabled

  判断是否支持全屏方法。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Boolean | 是/否 |  |

* 代码示例
```js
console.log($jstools.isFullscreenEnabled())
```

## isWeixinBrowser

  判断是否微信浏览器。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Boolean | 是/否 |  |

* 代码示例
```js
console.log($jstools.isWeixinBrowser())
```

## isMobileBrowser

  判断是否是移动端浏览器。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Boolean | 是/否 |  |

* 代码示例
```js
console.log($jstools.isMobileBrowser())
```

## screenIsLandscape

  判断浏览窗口是否是横屏（若有orientation则用其为依据，否则用页面宽高大小做依据）。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Boolean | 是/否 |  |

* 代码示例
```js
console.log($jstools.screenIsLandscape())
```

## screenIsPortrait

  判断浏览窗口是否是竖屏（与screenIsLandscape相对）。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Boolean | 是/否 |  |

* 代码示例
```js
console.log($jstools.screenIsPortrait())
```

## base64Decode

  base64解密处理器。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| base64Str | base64字符串 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 解密后的字符串 |  |

* 代码示例
```js
// 基于window.atob
console.log($jstools.base64Decode('aGk='))
```

## base642utf8

  base64字符串转utf8。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| base64Str | base64字符串 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 解码后的字符串 |  |

* 代码示例
```js
console.log($jstools.base642utf8('aGk='))
```

## utf82base64

  utf8转base64字符串。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| utf8Str | utf8字符串 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 转码后的字符串 |  |

* 代码示例
```js
console.log($jstools.utf82base64('支持中文'))
```

## dataUrl2File

  将数据字符串转换为文件对象。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| dataUrl | 数据字符串 | true | String |  |
| type | 要转换的目标类型，可选值1(file),2(blob) | false | Number | 2 |
| fileName | 转换后的文件名称，仅当type为1时生效 | false | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| File/Blob | 转换后的文件对象 |  |

* 代码示例
```js
console.log($jstools.dataUrl2File('data:image/ico;base64,AAABAAEAICD/AAAAAACoCAAAFgAAACgAAAAgAAAAQAAAAAEACAAAAAAAgAQAACMuAAAjLgAAAAAAAAAAAAD///8AhIB7AOvq6gDS12oAULdWAAv8swBWC6gAsFYLAFStVgALAKoAVgusAKZWCwBYo1YACwSgAFYLsACcVgsAXJlWAAsIlgBWC7QAklYLAGCPVgALDIwAVgu4AIhWCwBkhVYACxCCAFYLvAB+VgsAaHtWAAsUeABWC8AAdFYLAGxxVgALGG4AVgvEAGpWCwBwZ1YACxxkAFYLyABgVgsAdF1WAAsgWgBWC8wAVlYLAHhTVgALJFAAVgvQAExWCwB8SVYACyhGAFYL1ABCVgsAgD9WAAssPABWC9gAOFYLAIQ1VgALMDIAVgvcAC5WCwCIK1YACzQoAFYL4AAkVgsAjCFWAAs4HgBWC+QAGlYLAJAXVgALPBQAVgvoABBWCwCUDVYAC0AKAFYL7AAGVgsAmANWAAtEAABWC/AA/FULAJz5VQALSPYAVQv0APJVCwCg71UAC0zsAFUL+ADoVQsApOVVAAtQ4gBVC/wA3lULAKjbVQALVNgAVQsAANVVCwCs0VUAC1jOAFULBADLVQsAsMdVAAtcxABVCwgAwVULALS9VQALYLoAVQsMALdVCwC4s1UAC2SwAFULEACtVQsAvKlVAAtopgBVCxQAo1ULAMCfVQALbJwAVQsYAJlVCwDElVUAC3CSAFULHACPVQsAyItVAAt0iABVCyAAhVULAMyBVQALeH4AVQskAHtVCwDQd1UAC3x0AFULKABxVQsA1G1VAAuAagBVCywAZ1ULANhjVQALhGAAVQswAF1VCwDcWVUAC4hWAFULNABTVQsA4E9VAAuMTABVCzgASVULAORFVQALkEIAVQs8AD9VCwDoO1UAC5Q4AFULQAA1VQsA7DFVAAuYLgBVC0QAK1ULAPAnVQALnCQAVQtIACFVCwD0HVUAC6AaAFULTAAXVQsA+BNVAAukEABVC1AADVULAPwJVQALqAYAVQtUAANVCwAAAFUAC/QfAFgC9AAfWAIAgA0AAAD4GABYAogAGlgCABgcWAACzO0ApABcAO+kAAAMwVkAApCkAFYCrADbWgIAPKZWAALgBABbAlwABlsCANgHWwACVAkAWwLQAApbAgBoDFsAAsgNAFsCMAAPWwIAcMimAAC4DwBbAgAAyqYAAJDLpgAAIM0ApgCwAM6mAABAEFsAAkBFAKQAtABGpAAAMEikAABsIABYAmwAIFgCAAgNAAAAAAEAAAB8ACBYAgB8IFgAAigAAAAAAAAAAAAAAAAAAAAUrgBOAgAAAAAAAAgAAAD/YAAAAABIAAAAAAAjAAAAAHTzAFYAAAAAAAAAAAAAAAAPAAAA/wAAAAAAAOTfTgACAQEAAADEACBYAgDEIFgAAigAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAgEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAgICAgIBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQICAgICAgIBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAECAgICAgICAgIBAAAAAAAAAAAAAAAAAAAAAAAAAAABAgICAgICAgICAQMBAAAAAAAAAAAAAAAAAAAAAAAAAQICAgICAgICAgEDAwMBAAAAAAAAAAAAAAAAAAAAAAICAgICAgICAgIBAwMDAwMBAAAAAAAAAAAAAAAAAAACAgICAgICAgICAQMDAwMDAwMBAAAAAAAAAAAAAAAAAQICAgICAgICAgEDAwMDAwMDAwMBAAAAAAAAAAAAAAABAgICAgICAgIBAwMDAwMDAwMDAwMBAAAAAAAAAAAAAAACAgICAgICAQMDAwMDAwMDAwMDAwMBAAAAAAAAAAAAAAECAgICAgEDAwMDAwMDAwMDAwMDAwMBAAAAAAAAAAAAAAECAgIBAwMDAwMDAwMDAwMDAwMDAwMBAAAAAAAAAAAAAAACAgMDAwMDAwMDAwMDAwMDAwMDAQICAAAAAAAAAAAAAAEDAwMDAwMDAwMDAwMDAwMDAwECAgEAAAAAAAAAAAAAAAEDAwMDAwMDAwMDAwMDAwMBAgICAQAAAAAAAAAAAAAAAAEDAwMDAwMDAwMDAwMDAQICAgIBAAAAAAAAAAAAAAAAAAEDAwMDAwMDAwMDAwECAgICAQAAAAAAAAAAAAAAAAAAAAEDAwMDAwMDAwMBAgICAgEAAAAAAAAAAAAAAAAAAAAAAAEDAwMDAwMDAQICAgIBAAAAAAAAAAAAAAAAAAAAAAAAAAEDAwMDAwECAgICAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAEDAwMBAgICAgEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEDAQICAgIBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAECAgICAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAECAgEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/////////////////B////gP///wB///4AP//8AB//+AAP//AAB//gAAP/wAAB/8AAAP/gAAB/4AAAP/AAAB/8AAAP/AAAD/4AAA//AAAP/4AAH//AAD//4AB///AA///4Af///AP///4H////D////5//////////////////'))
```

## File2dataUrl

  File转换DataURL。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| file | 待转换的File对象 | Object |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 成功后返回DataURL地址 |  |

* 代码示例
```js
let file = new File(['TEST'], 'filename.txt', {type: 'text/plain'})
$jstools.File2dataUrl(file).then((url) => {console.log(url)})
```

## img2dataUrl

  Image DOM元素转换DataURL。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options | 参数配置 |  | Object |  |
| options/img | 待转换的img DOM元素 |  | Object |  |
| options/width | 转换后的图片宽度 |  | Number | 图片自身宽度 |
| options/height | 转换后的图片高度 |  | Number | 图片自身高度 |
| options/quality | 转换后的图片质量 |  | Number | 100 |
| options/mimeType | 图片的mimeType |  | String | image/png |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 返回DataURL地址 |  |

* 代码示例
```js
$jstools.loadImage('https://gitee.com/static/errors/images/403.png').then((imgObj) => {
    let base64url = $jstools.img2dataUrl({img: imgObj, mimeType: 'image/png'})
    console.log('img2dataUrl:', base64url)
})
```

## xml2dom

  将XML字符串解析为DOM对象。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| xmlStr | XML字符串 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Object | DOM对象 |  |

* 代码示例
```js
$jstools.xml2dom('<bookstore><book><title>Everyday Italian</title><author>Giada De Laurentiis</author><year>2005</year></book></bookstore>')
```

## dom2xml

  将XML DOM转换为XML字符串。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| xmlDom | XML DOM文档对象 | true | Object |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | XML字符串 |  |

* 代码示例
```js
let xmlDom = $jstools.xml2dom('<bookstore><book><title>Everyday Italian</title><author>Giada De Laurentiis</author><year>2005</year></book></bookstore>');
$jstools.dom2xml(xmlDom);
```

## relativeUrl2absoluteUrl

  相对地址转换绝对地址。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| url | 相对地址 | true | String |  |
| base | 基准地址 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 绝对地址 |  |

* 代码示例
```js
$jstools.relativeUrl2absoluteUrl('../22/demo.jpg', 'http://xxx.com/11/')
```

## arrayBuffer2audioBuffer

  将ArrayBuffer转换为AudioBuffer。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| arrayBuffer | ArrayBuffer数据 | true | ArrayBuffer |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 返回一个Promise对象，接收值为一个AudioBuffer |  |

## url2audioBuffer

  基于url地址获得 AudioBuffer 的方法。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| url | url地址 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 返回一个Promise对象，接收值为一个AudioBuffer |  |

## audioBuffer2wave

  将AudioBuffer数据转换为Wave。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| audioBuffer | AudioBuffer数据 | true | AudioBuffer |  |
| len | 采样长度 | false | Number | `audioBuffer.sampleRate * audioBuffer.duration` |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Blob | Wave Blob |  |

## concatAudioBufferList

  拼接AudioBuffer数据数组。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| audioBufferList | AudioBuffer数据数组 | true | AudioBuffer[] |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| AudioBuffer | 拼接后的新AudioBuffer |  |

## mergeAudioBufferList

  合并AudioBuffer数据数组。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| audioBufferList | AudioBuffer数据数组 | true | AudioBuffer[] |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| AudioBuffer | 合并后的新AudioBuffer |  |

## cutAudioBuffer

  音频裁剪。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| audioBuffer | AudioBuffer数据 | true | AudioBuffer |  |
| startOffset | 起始点的秒数 | true | Number |  |
| endOffset | 截止点的秒数 | true | Number |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| AudioBuffer | 裁剪后的新AudioBuffer |  |

## loadImage

  加载图片数据。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| param | 图片地址或File对象 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 返回一个Promise对象 |  |

* 代码示例
```js
$jstools.loadImage('https://gitee.com/static/images/logo-black.svg').then((imgObj) => {
  document.body.appendChild(imgObj)
})
```

## zoomScreenByBaseSize

  按基准大小缩放文档视口。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| baseSize | 基准大小 | true | Number |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.zoomScreenByBaseSize(720)
```

## getUrlParams

  获取Url地址中参数。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| url | Url地址 | false | String | 当前页面地址 |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Object | 参数键值对 |  |

* 代码示例
```js
console.log($jstools.getUrlParams())
```

## getSystemType

  获取系统类型。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 系统类型名称(Windows/UNIX/Linux/MacOS/Android/iOS) |  |

* 代码示例
```js
console.log($jstools.getSystemType())
```

## getVideoThumb

  获取视频缩略图。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.url | 视频url | true | String |  |
| options.timeStart | 截图的起始时间 | false | Number | 0.1(秒) |
| options.timeInterval | 截图的时间间隔 | false | Number | 1(秒) |
| options.onLoading | 视频开始加载 | false | Function |  |
| options.onLoaded | 视频加载完成 | false | Function |  |
| options.onFinish | 视频截图处理完毕，接收一个缩略图Blob数组 | true | Function | `function(arr){}` |

## myEvent

  事件方法集：事件绑定&触发&解除。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| eventName | 事件名称 | true | String |  |
| callback | 回调函数 | true | Function |  |
| isBubbling | 是否冒泡 | false | Boolean |  |
| cancelable | 是否阻止浏览器的默认行为 | false | Boolean |  |
| argument | 自定义传参 | false | Object |  |

* 方法
| 名称 | 说明 | 传参 | 返回值 |
| :----: | :-----| :-----| :-----|
| addEventListener | 事件绑定 | eventName, callback |  |
| removeEventListener | 事件解绑 | eventName, callback |  |
| dispatchEvent | 事件触发 | eventName, argument, isBubbling, cancelable |  |

* 代码示例
```js
let fn = (e) => {console.log(e.name)}
$jstools.myEvent.addEventListener('召唤兵器', fn)
$jstools.myEvent.dispatchEvent('召唤兵器', {name: 'jstools-common'})
$jstools.myEvent.removeEventListener('召唤兵器', fn)
```

## myCamera

  摄像头事件方法集：获取摄像头列表&获取摄像头视频流&获取视频流错误处理&关闭视频流。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.constraints | getStream/媒体参数配置 | true | Object |  |
| options.deviceId | getStream/媒体设备ID | true | String |  |
| options.mediaDom | getStream/video标签DOM元素 | false | Object |  |
| options.success | getStream/成功回调函数 | false | Function |  |
| options.fail | getStream/失败回调函数 | false | Function |  |
| options.oninactive/onended | getStream/数据流结束回调函数 | false | Function |  |
| err | getStreamError/错误对象 | false | Object |  |
| callback | getStreamError/回调函数 | false | Function |  |
| mediaStream | closeStream/视频流对象 | false | Object |  |

* 方法
| 名称 | 说明 | 传参 | 返回值 |
| :----: | :-----| :-----| :-----|
| getDevicesList | 获取摄像头列表 |  | Promise对象，接收一个数组参数 |
| getStreamError | 调用摄像头失败的错误信息处理 | err, callback |  |
| getStream | 获取摄像头视频流 | {constraints,deviceId,mediaDom,success,fail,oninactive/onended} | Promise对象 |
| closeStream | 关闭摄像头 | mediaStream |  |

* 代码示例
```js
$jstools.myCamera.getDevicesList().then((res) => {console.log(res)})
```

## myRecorder

  录音事件方法集：获取录音设备列表&获取录音设备数据流&获取音频流错误处理&关闭音频流。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.constraints | getStream/媒体参数配置 | true | Object |  |
| options.deviceId | getStream/媒体设备ID | true | String |  |
| options.mediaDom | getStream/audio标签DOM元素 | false | Object |  |
| options.success | getStream/成功回调函数 | false | Function |  |
| options.fail | getStream/失败回调函数 | false | Function |  |
| options.oninactive/onended | getStream/数据流结束回调函数 | false | Function |  |
| err | getStreamError/错误对象 | false | Object |  |
| callback | getStreamError/回调函数 | false | Function |  |
| mediaStream | closeStream/视频流对象 | false | Object |  |

* 属性
| 名称 | 说明 | 返回值 |
| :----: | :-----| :-----|
| stream | 数据流对象 |  |
| mediaRecorder | 录音器实例 | MediaRecorder实例 |
| chunks | 录音数据栈 | Blob数据数组 |
| mediaUrl | 录音结束后生成的URL地址 |  |

* 方法
| 名称 | 说明 | 传参 | 返回值 |
| :----: | :-----| :-----| :-----|
| getDevicesList | 获取录音设备列表 |  | Promise对象，接收一个数组参数 |
| getStreamError | 调用录音设备失败的错误信息处理 | err, callback |  |
| getStream | 获取录音设备音频流 | {constraints,deviceId,mediaDom,success,fail,oninactive/onended} | Promise对象 |
| closeStream | 关闭录音设备 | mediaStream |  |
| getMediaRecorder | 获取录音器实例 | {mediaStream,start(开始录音的回调函数),stop(录音结束的回调函数，接收一个参数，音频的URL地址),dataavailable(获取录制的媒体资源)} | Promise对象，接收一个参数(MediaRecorder实例) |

* 代码示例
```js
$jstools.myRecorder.getStream({mediaDom: document.querySelector('audio')})
```

## myCookie

  Cookie操作方法集。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| key | 参数名称 |  | String |  |
| value | 参数值 |  | String |  |
| options | 其它配置，如:expires,path,domain,secure |  | Object |  |

* 方法
| 名称 | 说明 | 传参 | 返回值 |
| :----: | :-----| :-----| :-----|
| getAll | 获取cookie信息 |  | 键值对数据 |
| getItem | 获取指定参数值 | key | 参数值 |
| setItem | 增加一个参数值 | key,value,options |  |
| removeItem | 删除指定参数 | key |  |
| clear | 清空cookie信息 |  |  |

* 代码示例
```js
$jstools.myCookie.setItem('user', 'baidu', {
    expires: 10, // 有效期为 10 天
    path: '/', // 整个站点有效
    domain: 'www.baidu.com', // 有效域名
    secure: true // 加密数据传输
})
```

## screenshot2pdf

  网页截图并保存为PDF文件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.catalogues | 封面对应的dom元素数组 | false | Array |  |
| options.contents | 内容页对应的dom元素数组 | false | Array |  |
| options.clipClass | 可裁切元素的类名 | false | String |  |
| options.flipClass | 需另起一页绘制的元素的类名 | false | String |  |
| options.fileName | 要保存的截图名称 | false | String |  |
| options.useCORS | htmlCanvas配置参数 | false | Boolean | false |
| options.orientation | 页面采用横向还是纵向 | false | String | landscape |
| options.offsetX | 页面X轴偏移量 | false | Number | undefined |
| options.offsetY | 页面Y轴偏移量 | false | Number | undefined |
| options.paperWidth | 页面宽度（默认参考A4纸） | false | Number | 297 |
| options.paperHeight | 页面高度（默认参考A4纸） | false | Number | 210 |
| options.availWidth | 有效宽度（默认参考A4纸） | false | Number | 287 |
| options.availHeight | 有效高度（默认参考A4纸） | false | Number | 185 |
| options.pageStartTop | 页面内容纵向开始位置（默认参考A4纸） | false | Number | 10 |
| options.pageStartLeft | 页面内容横向开始位置（默认参考A4纸） | false | Number | 5 |
| options.pageNumTop | 页码纵向位置（默认参考A4纸） | false | Number | 280 |
| options.pageNumLeft | 页码横向位置（默认参考A4纸） | false | Number | 205 |
| options.fontSize | 文本字号大小 | false | Number | 12 |
| options.html2canvas | html2canvas第三方库方法 | true | Function | window.html2canvas |
| options.jsPDF | jsPDF第三方库方法 | true | Function | window.jspdf.jsPDF |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象 |  |

* 代码示例
```js
$jstools.screenshot2pdf({contents: document.querySelectorAll('.page')})
```

## screenshot2pdfV2

  网页截图并保存为PDF文件，有别于screenshot2pdf的是：该方法更适用于打印页面较多的情况，但花费时间较长。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.elements | PDF页对应的dom元素数组 | false | Array |  |
| options.coverClass | 封面元素的类名 | false | String |  |
| options.clipClass | 可裁切元素的类名 | false | String |  |
| options.flipClass | 需另起一页绘制的元素的类名 | false | String |  |
| options.fileName | 要保存的截图名称 | false | String |  |
| options.useCORS | htmlCanvas配置参数 | false | Boolean | false |
| options.orientation | 页面采用横向还是纵向 | false | String | landscape |
| options.offsetX | 页面X轴偏移量 | false | Number | undefined |
| options.offsetY | 页面Y轴偏移量 | false | Number | undefined |
| options.paperWidth | 页面宽度（默认参考A4纸） | false | Number | 297 |
| options.paperHeight | 页面高度（默认参考A4纸） | false | Number | 210 |
| options.availWidth | 有效宽度（默认参考A4纸） | false | Number | 287 |
| options.availHeight | 有效高度（默认参考A4纸） | false | Number | 185 |
| options.pageStartTop | 页面内容纵向开始位置（默认参考A4纸） | false | Number | 10 |
| options.pageStartLeft | 页面内容横向开始位置（默认参考A4纸） | false | Number | 5 |
| options.pageNumTop | 页码纵向位置（默认参考A4纸） | false | Number | 280 |
| options.pageNumLeft | 页码横向位置（默认参考A4纸） | false | Number | 205 |
| options.fontSize | 文本字号大小 | false | Number | 12 |
| options.html2canvas | html2canvas第三方库方法 | true | Function | window.html2canvas |
| options.jsPDF | jsPDF第三方库方法 | true | Function | window.jspdf.jsPDF |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象 |  |

* 代码示例
```js
$jstools.screenshot2pdfV2({elements: document.querySelectorAll('.page')})
```

## polyfillCustomEvent

  CustomEvent构造器Polyfill。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.polyfillCustomEvent()
```

## polyfillRequestAnimationFrame

  requestAnimationFrame方法的Polyfill。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.polyfillRequestAnimationFrame()
```

## polyfillCancelAnimationFrame

  cancelAnimationFrame方法的Polyfill。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
$jstools.polyfillCancelAnimationFrame()
```
