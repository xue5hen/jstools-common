  该类方法仅能在Node环境下使用。

## getJson

  JSON文件读取。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| filePath | 源地址 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | Promise接收一个参数（解析后的JSON数据） |  |

* 代码示例
```js
getJson('D:\\1.json')
```

## copyFile

  文件拷贝。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| from | 源地址 | true | String |  |
| to | 目标地址 | true | String |  |
| streammode | 是否使用流传输 | false | Boolean | false |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
copyFile('D:\\1.json', 'D:\\2.json')
```

## copyDir

  目录拷贝。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| from | 源地址 | true | String |  |
| to | 目标地址 | true | String |  |
| replace | 是否覆盖同名文件 | false | Boolean | true |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
copyDir('D:\\1', 'D:\\2')
```

## mkDir

  创建目录。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| filePath | 目标路径 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
mkDir('D:\\1')
```

## delFile

  删除文件/文件夹。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| filePath | 目标路径 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
delFile('D:\\1')
```

## writeFileSync

  将数据写入文件（同步）。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| filePath | 目标路径 | true | String |  |
| bufferData | 数据 | true | Buffer |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| 无 |  |  |

* 代码示例
```js
writeFileSync('D:\\1\\1.txt', '123')
```

## writeFile

  将数据写入文件（异步）。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| filePath | 目标路径 | true | String |  |
| bufferData | 数据 | true | Buffer |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象 |  |

* 代码示例
```js
writeFile('D:\\1\\1.txt', '123')
```

## writeFileV2

  将分片数据异步写入文件。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| taskList | Promise数组，每个Promise返回一个Buffer数据 | true | Array |  |
| filePath | 目标路径 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象 |  |

* 代码示例
```js
// 下载大文件时容易内存溢出，该方法就是为了解决该问题，用类似数据流的方式依次将数据写入到本地文件
let fs = require('fs')
let pathList = ['D:\\01.ts', 'D:\\02.ts', 'D:\\03.ts']
let taskList = pathList.map(v => {
  return new Promise((resolve, reject) => {
    fs.readFile(v, function (err, data) { resolve(data) })
  })
})
writeFileV2(taskList, 'D:\\合并.mp4')
```

## readDir

  读取目录。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| filePath | 目标路径 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象，接收参数为文件列表 |  |

* 代码示例
```js
readDir('D:\\1')
```

## getFileMd5

  获取文件MD5值。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| filePath | 目标路径 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| String | 文件MD5值 |

* 代码示例
```js
console.log('文件MD5值：', getFileMd5('D:\\1.json'))
```

## uploadFile

  上传文件到服务器。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 源地址 | true | String |  |
| options.to | 目标地址 | true | String |  |
| options.contentType | 数据类型 | false | String | application/octet-stream |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象，接收参数为接口返回值 |  |

* 代码示例
```js
uploadFile({
    from: 'D:\\1.json',
    to: 'x.com/upload'
})
```

## downloadFile

  下载文件到本地，基于axios，responseType为blob。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 源地址 | true | String |  |
| options.to | 目标地址 | true | String |  |
| options.progressCallback | 进度回调函数 | false | Function |  |
| options.config | 其它配置 | false | Object |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象 |  |

* 代码示例
```js
downloadFile({
    from: 'x.com/1.pdf',
    to: 'D:\\new.pdf'
})
```

## downloadFileV2

  下载文件到本地，基于axios，与downloadFile仅在responseType上有所不同。

## downloadFileV3

  下载文件到本地，基于request模块。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 源地址 | true | String |  |
| options.to | 目标地址 | true | String |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象 |  |

* 代码示例
```js
downloadFileV3({
    from: 'x.com/1.pdf',
    to: 'D:\\new.pdf'
})
```

## encrypt

  加密数据。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| key | 密钥 | true | String |  |
| bufferData | 数据 | true | Buffer |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Buffer | 加密后的数据 |  |

* 代码示例
```js
encrypt(Buffer.from('l6fwZHdJbU2Y4jxPDwjoI6P9vltHhc8bvDlExm4vRRg=', 'base64'), fs.readFileSync('D:\\1.json'))
```

## zipFile

  压缩&加密课件资源包。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 待压缩加密的ZIP文件或文件夹 | true | String |  |
| options.to | 压缩&加密后文件存放路径 | true | String |  |
| options.JSZip | JSZip第三方库方法 | true | Function | window.JSZip |
| options.encryptKey | 加密密钥 | false | Buffer |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象 |  |

* 代码示例
```js
zipFile({
    from: 'D:\\1',
    to: 'D:\\2'
})
```

## unzipFile

  解压（加密）课件资源包。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options.from | 源地址 | true | String |  |
| options.to | 目标地址 | true | String |  |
| options.JSZip | JSZip第三方库方法 | true | Function | window.JSZip |
| options.encryptKey | 解密密钥 | false | Buffer |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象 |  |

* 代码示例
```js
unzipFile({
    from: 'D:\\1.zip',
    to: 'D:\\2'
})
```

## getIpAddress

  获取本机IP地址。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| 无 |  |  |  |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Array | IP地址数组 |  |

* 代码示例
```js
getIpAddress()
```

## portIsOccupied

  检测端口是否被占用。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| port | 端口号 | true | Number |  |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象，接收参数为Boolean值 |  |

* 代码示例
```js
portIsOccupied(8080).then((res) => {console.log(res)})
```

## getAvailablePorts

  获取可使用的端口号。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| s_port | 起始端口号 | false | Number | 1 |
| count | 需要获取的端口数量 | false | Number | 1 |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象，接收参数为端口数组 |  |

* 代码示例
```js
getAvailablePorts().then((res) => {console.log(res)})
```

## getNetspeedByPing

  使用ping命令测算网速。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options/website | 要ping的网站 | false | String | baidu.com |
| options/bytecount | 字节数 | false | Number | 1000 |
| options/encode | 编码处理 | false | String | binary |
| options/decode | 解码处理 | false | String | cp936 |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象，接收参数为测速结果String | 103.29KB/s |

* 代码示例
```js
getNetspeedByPing().then((res) => {console.log(res)})
```

## getRouteInterfaceList

  获取路由接口列表。

* 参数
| 名称 | 说明 | 必须 | 类型 | 默认值 |
| :-----| :-----| :----: | :----: | :----: |
| options/encode | 编码处理 | false | String | binary |
| options/decode | 解码处理 | false | String | cp936 |
| options/blacklist | 黑名单，排除名称包含非法字符的网卡 | false | Array | [] |

* 返回值
| 类型 | 说明 | 示例 |
| :----: | :-----| :-----|
| Promise | 一个Promise对象，接收数据为接口列表Array | [{ id: '8', name: 'VirtualBox Host-Only Ethernet Adapter', mac: '0a 00 27 00 00 08' }, { id: '17', name: 'Realtek PCIe GbE Family Controller', mac: '1c 1b 0d 74 18 91' }] |

* 代码示例
```js
getRouteInterfaceList({blacklist: ['VirtualBox', 'Loopback']}).then((res) => {console.log(res)})
```
